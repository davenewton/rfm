# gc2

A simple javascript based coincoin. It is a component of [gb3](https://gitlab.com/davenewton/gb3).

## Links

[Main instance](https://gb3.devnewton.fr/)

[Source repository](https://gitlab.com/davenewton/gb3)