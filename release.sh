#!/usr/bin/env sh

RFM_VERSION=1.3

GITLAB_HOST="gitlab.com"
GITLAB_USER="davenewton"

curl -u "${GITLAB_USER}:${GITLAB_TOKEN}" --upload-file rfm.tar.gz https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Frfm/packages/generic/rfm/${RFM_VERSION}/rfm.tar.gz | tee /dev/null

curl --request POST "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Frfm/releases" \
     --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data @- <<REQUEST_BODY
{
  "name": "${RFM_VERSION}",
  "tag_name": "${RFM_VERSION}",
  "ref": "master",
  "description": "Release ${RFM_VERSION}",
  "assets": {
    "links": [
      {
        "name": "rfm.tar.gz",
        "url": "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_USER}%2Frfm/packages/generic/rfm/${RFM_VERSION}/rfm.tar.gz",
        "filepath": "/rfm.tar.gz",
        "link_type": "package"
      }
    ]
  }
}
REQUEST_BODY
